use crate::error::Result;
use nix::{
    unistd::{fork, ForkResult, Pid},
};

pub struct Server<T, FChild>
where
    FChild: Fn(u32, T),
{
    num_processes: u32,
    resource: Option<T>,
    child_init: FChild,
}

impl<T, FChild> Server<T, FChild>
where
    FChild: Fn(u32, T),
{
    pub fn from_resource(resource: T, child_init: FChild, num_processes: u32) -> Self {
        Self {
            num_processes,
            resource: Some(resource),
            child_init,
        }
    }

    pub fn fork(mut self) -> Result<bool> {
        for child_num in 0..self.num_processes {
            match self.fork_child(child_num)? {
                Some(_) => continue,
                None => return Ok(false),
            }
        }
        Ok(true)
    }

    fn fork_child(&mut self, child_num: u32) -> Result<Option<Pid>> {
        match unsafe { fork()? } {
            ForkResult::Parent { child } => Ok(Some(child)),
            ForkResult::Child => {
                if let Some(resource) = self.resource.take() {
                    (self.child_init)(child_num, resource);
                } else {
                    unreachable!("fork resource is empty");
                }
                Ok(None)
            }
        }
    }
}
