use std::{net::TcpListener, process};

use axum::{extract::State, routing::get, Router};

use log::info;
use prefork::Prefork;

async fn child(child_num: u32, listener: TcpListener) {
    let pid = process::id();
    let router = Router::new()
        .route(
            "/",
            get(|State((pid, child_num))| async move {
                format!("Hello from {child_num} with pid {pid}")
            }),
        )
        .with_state((pid, child_num));
    axum::Server::from_tcp(listener)
        .expect("cannot create server")
        .serve(router.into_make_service())
        .await
        .expect("cannot start server")
}

fn main() {
    env_logger::init();
    let listener = TcpListener::bind("0.0.0.0:3000").expect("cannot bind to port");
    let is_parent = Prefork::from_resource(listener)
        .with_num_processes(10)
        .with_tokio(child)
        .fork()
        .expect("cannot fork");
    if is_parent {
        info!("Parent exit");
    }
}
